from django.urls import include, path
from django.contrib import admin
from django.contrib.auth import views as auth_views

urlpatterns = [
    # Examples:
    # url(r'^$', 'confAssignments.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    # path('accounts/login/', auth_views.LoginView.as_view(), name='login'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('', include('conference.urls')),
    path('admin/', admin.site.urls),
]
