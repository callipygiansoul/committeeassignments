The Committee Assignment App is currently a demonstration of assigning a new panel of delegates to their committees.  It was written in Django on Python3.  Once you get the app up and running, you will see a short description of what the application does on the web page.  You can currently also go [here](http://callipygiansoul.pythonanywhere.com/) to use the demo.

###Getting started

####Installation
Installation instructions are for the command line and have only been tested on a mac.  You should have virtualenv and Python3 installed.  Activation of the virtual environment is done for the bash-like shell but can be done for other shells as well.

__Create the operating environment__
```
git clone https://bitbucket.org/callipygiansoul/committeeassignments.git
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt 
cp committeeassignments/settings_local_example.py committeeassignments/settings_local.py
```

__Populate the Database__
```
./manage.py migrate
./manage.py loadSampleData conference/confData/areaData.yaml conference/confData/committeeData.yml conference/sampleDelegates/sampleDelegates.tab True
./manage.py runserver
```

You should be able to open localhost:8000 in your browser and see the demo page.

####Loading initial data
If you want to load the demo data run the commands below.

First, if it doesn't exist, create the pickle file that contains the list of adjacent areas.  

`./manage.py createAdjGraph`

Then load committee and area data

`./manage.py laodSampleData --committeedata --areadata`

If desired, load the sample Panel 66 and 67 data (some of which is likely inaccurate)
`./manage.py loadSampleData --delegatedata`