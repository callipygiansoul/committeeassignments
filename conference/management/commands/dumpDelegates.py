from django.core.management.base import BaseCommand
import yaml
from conference.models import Delegate


class Command(BaseCommand):

    help = "Dump a file of delegates"

    # def add_arguments(self, parser):
    #     parser.add_argument('adjacentAreas')

    def handle(self, *args, **options):
        delegates = {}
        for delegate in Delegate.objects.all():
            delegate_data = {
                'name': delegate.name,
                'area': delegate.area.areaNumber,
                'gender': delegate.gender
            }
            if delegate.panel in delegates:
                delegates[delegate.panel].append(delegate_data)
            else:
                delegates[delegate.panel] = [delegate_data]
        with open('delegateDump.yml', 'w') as fh:
            yaml.dump(delegates, fh)

