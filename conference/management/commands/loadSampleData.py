import sys
import random
import yaml

from django.core.management.base import BaseCommand
from conference.models import Area, Committee, Delegate
from django.core.exceptions import ValidationError

from conference.conference import Conference
from conference.views import load_delegates


class Command(BaseCommand):

    help = "Import sample data"

    def add_arguments(self, parser):
        parser.add_argument('--areadata')
        parser.add_argument('--committeedata')
        parser.add_argument('--delegatedata')
        parser.add_argument('--nuke')

    def handle(self, *args, **options):

        if options['nuke']:

            Delegate.objects.all().delete()
            Committee.objects.all().delete()
            Area.objects.all().delete()

        if options['committeedata']:
            with open(options['committeedata'], 'r') as fh:
                try:
                    committee_data = yaml.load(fh)
                except Exception as e:
                    print(e)

                for committee in committee_data:
                    c, created = Committee.objects.update_or_create(name=committee['name'], defaults={
                        'display_name': committee['display_name'],
                        'short_display_name': committee['short_display_name'],
                        'max_members': committee['max_members'],
                    })
                    if created:
                        print('Loaded', committee['name'])

        if options['areadata']:
            with open(options['areadata'], 'r') as fh:
                try:
                    area_data = yaml.load(fh)
                except Exception as e:
                    print(e)
                for area in area_data:
                    Area.objects.get_or_create(areaNumber=area['areaNumber'], defaults={
                            'name': area['name'],
                            'state': area['state'],
                            'region': area['region'],
                            'cycle': area['cycle'],
                            'past_committees': ','.join(area['past_committees']),
                    })
                    print('Loaded', area['name'])

        if options['delegatedata']:
            Delegate.objects.all().delete()
            load_delegates(options['delegatedata'])
            print("{} delegates loaded".format(Delegate.objects.count()))