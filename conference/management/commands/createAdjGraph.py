from django.core.management.base import BaseCommand
import os
import yaml
import networkx as nx


class Command(BaseCommand):

    help = "Create Area Adjacency Graph"

    def add_arguments(self, parser):
        parser.add_argument('adjacentAreas')

    def handle(self, *args, **options):
        
        with open(options['adjacentAreas'], 'r') as fh:    
            adjData = {}
            try:
                adjData = yaml.load(fh)
            except Exception as e:
                print(e)
            
            G = nx.Graph()
            for key in adjData:
                G.add_node(key)
                for n in adjData[key]:
                    G.add_node(n)
                    G.add_edge(key, n)
            print('nodes count', G.number_of_nodes())
            print('edges count', G.number_of_edges())
            targetDir = os.path.dirname(options['adjacentAreas'])
            targetPath = os.path.join(targetDir, 'adjacentAreas.pickle')
            nx.write_gpickle(G, targetPath)
    