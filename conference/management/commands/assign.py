import os
import networkx as nx

from django.core.management.base import BaseCommand
from django.conf import settings

from conference.models import Area, Committee, Delegate
from conference.conference import Conference


class Command(BaseCommand):

    help = "Play around with the data"

    def add_arguments(self, parser):
        parser.add_argument('-r')

    def handle(self, *args, **options):

        conf = Conference([65, 66])
        
        graph = nx.read_gpickle(os.path.join(settings.BASE_DIR, 'conference/confData/adjacentAreas.pickle'))

        areas_in_region = {}
        for a in Area.objects.all():
            try:
                areas_in_region[a.region] += 1
            except KeyError:
                areas_in_region[a.region] = 1

        for key in sorted(areas_in_region.keys()):
            print(str(key)+":", areas_in_region[key])
        print('total', Area.objects.count())

        total_committee_headcount = 0
        for c in Committee.objects.all():
            total_committee_headcount += c.max_members
            print(c.name, c.max_members)
        print('total: ', total_committee_headcount)

        delegates = Delegate.objects.filter(panel=66).order_by('?')
        for delegate in delegates:
            delegate.committee.clear()
        unassigned = conf.assignment_looper(delegates, 'full')
        print('unassigned', unassigned)

        print("All Done!  All Delegates have been assigned")
