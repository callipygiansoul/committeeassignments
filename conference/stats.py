from .models import Committee, Delegate, Area

class Stats():

    @property
    def fPercent(self, panels):
        fPercent = Delegate.objects.filter(gender='F').count() / Delegate.objects.count()

