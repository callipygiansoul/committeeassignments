import os
import yaml
import networkx as nx
from django.test import TestCase
from django.conf import settings

from conference.tests.factories import DelegateFactory, CommitteeFactory, AreaFactory
from conference.models import Committee, Delegate, Area


def load_area_data():
    with open(os.path.join(settings.BASE_DIR, 'conference/confData/areaData.yaml'), 'r') as fh:
        areas_data = yaml.load(fh)
        for area_data in areas_data:
            Area.objects.create(**area_data)


class CommitteeTest (TestCase):
    def setUp(self):
        self.G = nx.read_gpickle(os.path.join(settings.BASE_DIR, 'conference/confData/adjacentAreas.pickle'))
        self.test_committee = CommitteeFactory()
        self.test_committee.save()
        load_area_data()

    def test_non_adjacent_area_list(self):
        non_adjacent_areas = [1, 2, 3, 4]

        for area_num in non_adjacent_areas:
            area = Area.objects.get(areaNumber=area_num)
            delegate = DelegateFactory(area=area)
            delegate.save()
            delegate.committee.add(self.test_committee)

        self.assertEquals(len(self.test_committee.adjacent_area_list(self.G)), 0)

    def test_adjacent_area_list(self):
        non_adjacent_areas = [1, 2, 3, 4, 16]

        for area_num in non_adjacent_areas:
            area = Area.objects.get(areaNumber=area_num)
            delegate = DelegateFactory(area=area)
            delegate.save()
            delegate.committee.add(self.test_committee)

        self.assertSetEqual(self.test_committee.adjacent_area_list(self.G), {1, 16})
