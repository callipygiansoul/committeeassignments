import os
import yaml
import networkx as nx
from django.test import TestCase
from django.conf import settings

import factory
from conference.tests.factories import DelegateFactory, CommitteeFactory, AreaFactory
from conference.models import Committee, Delegate, Area
from conference.conference import CommitteeBuilder, Conference


def load_area_data():
    with open(os.path.join(settings.BASE_DIR, 'conference/confData/areaData.yaml'), 'r') as fh:
        areas_data = yaml.load(fh)
        for area_data in areas_data:
            Area.objects.create(**area_data)


class CommitteeBuilderTest (TestCase):
    def setUp(self):
        self.test_committee = CommitteeFactory()
        self.adjacency_graph = nx.read_gpickle(os.path.join(settings.BASE_DIR, 'conference/confData/adjacentAreas.pickle'))
        self.conference_obj = Conference((66, 67))
        load_area_data()


    def test_adjancency_count(self):
        builder = CommitteeBuilder(self.test_committee, self.adjacency_graph, .40)
        delegate1 = DelegateFactory(area=Area.objects.get(areaNumber=20))
        builder.add_delegates([delegate1])
        delegate2 = DelegateFactory(area=Area.objects.get(areaNumber=54))
        self.assertEquals(builder.adjacency_count(delegate2), 0)

        builder.add_delegates([delegate2])
        delegate3 = DelegateFactory(area=Area.objects.get(areaNumber=55))
        self.assertEquals(builder.adjacency_count(delegate3), 1)

        builder.add_delegates([delegate3])
        delegate4 = DelegateFactory(area=Area.objects.get(areaNumber=56))
        self.assertEquals(builder.adjacency_count(delegate4), 2)

        builder.add_delegates([delegate4])
        delegate5 = DelegateFactory(area=Area.objects.get(areaNumber=53))
        self.assertEquals(builder.adjacency_count(delegate5), 3)

    def test_past_committees(self):
        delegate = DelegateFactory(area=AreaFactory())
        builder = CommitteeBuilder(CommitteeFactory(name='archives'), self.adjacency_graph, .5)
        self.assertEquals(builder.recent_assignment_index(delegate), 0)

        builder = CommitteeBuilder(CommitteeFactory(name='pi'), self.adjacency_graph, .5)
        self.assertEquals(builder.recent_assignment_index(delegate), 1)

        builder = CommitteeBuilder(CommitteeFactory(name='treatment'), self.adjacency_graph, .5)
        self.assertEquals(builder.recent_assignment_index(delegate), 3)

        builder = CommitteeBuilder(CommitteeFactory(name='policy'), self.adjacency_graph, .5)
        self.assertIsNone(builder.recent_assignment_index(delegate))

    def test_same_region_count(self):
        builder = CommitteeBuilder(self.test_committee, self.adjacency_graph, .40)
        area58 = Area.objects.get(areaNumber=58)
        area28 = Area.objects.get(areaNumber=28)
        delegate1 = DelegateFactory(area=area58)
        delegate2 = DelegateFactory(area=area28)
        builder.add_delegates([delegate1])
        self.assertEqual(builder.same_region_count(delegate2), 0)

        builder.add_delegates([delegate2])
        area72 = Area.objects.get(areaNumber=72)
        delegate3 = DelegateFactory(area=area72)
        self.assertEqual(builder.same_region_count(delegate3), 1)

        builder.add_delegates([delegate3])
        area92 = Area.objects.get(areaNumber=92)
        delegate4 = DelegateFactory(area=area92)
        self.assertEqual(builder.same_region_count(delegate4), 2)

    def test_gender_counts(self):
        male_delegates = factory.create_batch(DelegateFactory, 3, gender='M', committees=[self.test_committee])
        female_delegates = factory.create_batch(DelegateFactory, 4, gender='F',committees=[self.test_committee])
        builder = CommitteeBuilder(self.test_committee, self.adjacency_graph, .40)
        self.assertTupleEqual(builder.gender_counts, (4,3))

    def test_max_members(self):
        committee = CommitteeFactory()
        delegates = factory.create_batch(DelegateFactory, 3)
        builder = CommitteeBuilder(committee, self.adjacency_graph, .4)
        builder.add_delegates(delegates[:1])

        validation_results = builder.validate(delegates[1], self.conference_obj.escalator_thresholds)
        self.assertTrue(validation_results[self.conference_obj.MAX_MEMBER_KEY])

        builder.add_delegates(delegates[1:2])
        validation_results = builder.validate(delegates[2], self.conference_obj.escalator_thresholds)
        self.assertFalse(validation_results[self.conference_obj.MAX_MEMBER_KEY])

    def test_validation(self):
        committee = CommitteeFactory(name='pi')
        delegate1 = DelegateFactory(area=Area.objects.get(areaNumber=58), gender='M', committees=[committee])
        delegate2 = DelegateFactory(area=Area.objects.get(areaNumber=44), gender='F', committees=[committee])
        delegate3 = DelegateFactory(area=AreaFactory(areaNumber=18, past_committees='cpc,treatment,archives,pi'), gender='F')
        delegate4 = DelegateFactory(area=AreaFactory(areaNumber=92, past_committees='cpc,treatment,archives,pi'),
                                    gender='F')
        builder = CommitteeBuilder(committee, self.adjacency_graph, .5)

        target_result = {
            self.conference_obj.GENDER_KEY: False,
            self.conference_obj.ADJACENCY_KEY: False,
            self.conference_obj.REGION_KEY: True,
            self.conference_obj.RECENTLY_ASSIGNED_KEY: False,
            self.conference_obj.MAX_MEMBER_KEY: False,
        }
        validation_result = builder.validate(delegate3, self.conference_obj.escalator_thresholds)
        self.assertDictEqual(
            validation_result, target_result,
            'vresult = {}, target={}'.format(validation_result, target_result)
        )

        target_result = {
            self.conference_obj.GENDER_KEY: False,
            self.conference_obj.ADJACENCY_KEY: True,
            self.conference_obj.REGION_KEY: True,
            self.conference_obj.RECENTLY_ASSIGNED_KEY: False,
            self.conference_obj.MAX_MEMBER_KEY: False,
        }
        validation_result = builder.validate(delegate3, self.conference_obj.escalator_thresholds, iteration=3)
        self.assertDictEqual(target_result, validation_result)


        target_result = {
            self.conference_obj.GENDER_KEY: True,
            self.conference_obj.ADJACENCY_KEY: True,
            self.conference_obj.REGION_KEY: True,
            self.conference_obj.RECENTLY_ASSIGNED_KEY: True,
            self.conference_obj.MAX_MEMBER_KEY: False,
        }
        validation_result = builder.validate(delegate3, self.conference_obj.escalator_thresholds, iteration=4)
        self.assertDictEqual(target_result, validation_result)

