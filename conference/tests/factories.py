
import factory

from conference.models import Committee, Delegate, Area


class CommitteeFactory (factory.django.DjangoModelFactory):
    class Meta:
        model = Committee

    name = factory.Sequence(lambda n: 'Committee {}'.format(n))
    display_name = factory.Sequence(lambda n: 'Committee {}'.format(n))
    max_members = 2


class AreaFactory (factory.django.DjangoModelFactory):
    class Meta:
        model = Area

    areaNumber = factory.Sequence(lambda n: n)
    name = 'Area Name'
    state = 'AK'
    region = 'nw'
    cycle = 'odd'
    past_committees = 'archives,pi,cpc,treatment'


class DelegateFactory (factory.django.DjangoModelFactory):
    class Meta:
        model = Delegate

    area = factory.Iterator(Area.objects.all())
    name = factory.LazyAttribute(lambda obj: obj.area.areaNumber)
    panel = 67
    gender = factory.Iterator(['M', 'F'])

    @factory.post_generation
    def committees(obj, create, extracted, **kwargs):
        if not create:
            return
        committees = extracted or []
        for committee in committees:
            obj.committee.add(committee)
