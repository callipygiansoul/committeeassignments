import os
import yaml

from django.shortcuts import render
from django.db.models import Count, Q
from django.contrib.auth.decorators import login_required
from conference.models import Delegate, Area, Committee
from conference.conference import Conference
from django.conf import settings


@login_required
def index(request):
    panels = (66, 67)
    conf = Conference(panels)

    panel2_has_been_assigned = Delegate.objects.filter(panel=panels[1]).first().committee.exists()
    unassigned_message = "Hit the Assign button to assign Panel {} to committees or the Reload button to reload the actual Panel {} assignments.".format(panels[1], panels[1])
    assigned_message = "Hit one of the buttons above to take further action."
    if panel2_has_been_assigned:
        message = "Panel {} has been assigned. {}".format(panels[1], assigned_message)
    else:
        message = "Panel {} is unassigned. {}".format(panels[1], unassigned_message)

    delegates = Delegate.objects.filter(panel=panels[1])
    request_type = 'get'
    if request.method == 'POST':
        request_type = 'post'
        if request.POST['action'] == 'assign':
            for d in delegates:
                if d.committee.count() > 0:
                    d.committee.clear()
            iterations, unassigned = conf.assignment_looper(delegates)
            if unassigned:
                message = "Tried {} iterations but still couldn't assign all delegates to committees.  Hit the Clear button and then the Assgin button to try again."
                message = message.format(iterations)
            else:
                if iterations <= 4:
                    message = "Success!  It took {} iterations to assign all the delegates. You can see the assignments by scrolling down the page."
                else:
                    message = "Success (sort of)!  It took {} iterations to assign all the delegates.  An iteration count of greater than 4 usually means the restrictions had to be relaxed by quite a bit to assign all of the delegates.  You may want to hit the Assign button to try again."
                message = message.format(iterations)
        elif request.POST['action'] == 'clear':
            message = "Panel {} was cleared. ".format(panels[1]) + unassigned_message
            for d in delegates:
                if d.committee.count() > 0:
                    d.committee.clear()
        elif request.POST['action'] == 'reload':
            message = "Delegates were reloaded.  {}".format(assigned_message)
            load_delegates()

    female_count = Delegate.objects.filter(gender='F').count()
    male_count = Delegate.objects.filter(gender='M').count()

    context = dict()
    context['panel1'] = {'delegates': Delegate.objects.filter(panel=panels[0]).order_by('area__areaNumber', 'name'), 'panel_number': panels[0]}
    context['panel2'] = {'delegates': Delegate.objects.filter(panel=panels[1]).order_by('area__areaNumber', 'name'), 'panel_number': panels[1]}
    context['composition'] = conf.committee_composition
    context['gender_count_totals'] = (female_count, male_count)
    context['message'] = message
    context['request_type'] = {'rtype': request_type}

    return render(request, 'conference/delegates.html', context=context)


def load_delegates(delegate_file=None):
    if not delegate_file:
        delegate_file = os.path.join(settings.BASE_DIR, 'conference/confData/delegateData.yaml')
    Delegate.objects.all().delete()
    with open(delegate_file, 'r') as fh:
        delegate_data = yaml.load(fh)
        for panel in delegate_data:
            for params in delegate_data[panel]:
                area = Area.objects.get(areaNumber=params['area'])
                committee = Committee.objects.get(name=params['committee'])
                # Need to get or create because the see data repeats some delegates since it's listed by committee
                delegate, created = Delegate.objects.get_or_create(
                    area=area,
                    name=params['name'],
                    panel=panel,
                    defaults={'gender': params['gender']}
                )

                delegate.committee.add(committee)
                delegate.save()
