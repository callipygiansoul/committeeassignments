import os
from functools import reduce
from operator import add
import math
from itertools import chain


import networkx as nx

from django.conf import settings

from .models import Committee, Delegate, Area


class Conference(object):

    GENDER_KEY = 'gender'
    ADJACENCY_KEY = 'adjacency'
    REGION_KEY = 'region'
    RECENTLY_ASSIGNED_KEY = 'recently_assigned'
    MAX_MEMBER_KEY = 'max_members'

    def __init__(self, panels):
        self.panels = panels
        self.adjacent_areas = nx.read_gpickle(
            os.path.join(settings.BASE_DIR, 'conference/confData/adjacentAreas.pickle'))
        self.committee_builders = []
        self.reset_committees()
        # Set the iteration count after which the restrictions on each category will be loosened
        self.escalator_thresholds = {
            self.GENDER_KEY: 3,
            self.ADJACENCY_KEY: 2,
            self.REGION_KEY: 2,
            self.RECENTLY_ASSIGNED_KEY: 3,
        }

    def reset_committees(self):
        self.committee_builders = []
        for committee in Committee.objects.all().order_by('?'):
            self.committee_builders.append(CommitteeBuilder(committee, self.adjacent_areas, self.female_percent))

    def assignment_looper(self, delegates_to_assign):

        """
        Try to assign delegates.  May need to make several attempts since random assignment may lead
        to some delegates being unassignable.  The loop uses the first and second runs is a way to
        prioritize delegates that could not be placed in the prior assignment round, on the theory that
        they may be particularly difficult to place.
        """

        hard_to_place = []
        iterations = 1
        unassigned = []
        for i in range(10):
            # print('\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n')
            # First try the hard-to-place delegates, then try the rest
            first_run = delegates_to_assign.filter(pk__in=hard_to_place).order_by('?')
            second_run = delegates_to_assign.exclude(pk__in=hard_to_place).order_by('?')
            first_fails = self.assign_delegates(first_run, iteration=i)
            second_fails = self.assign_delegates(second_run, iteration=i)
            hard_to_place.extend(first_fails)
            hard_to_place.extend(second_fails)

            unassigned = first_fails + second_fails
            # print('unassigned: ', unassigned)
            
            if len(unassigned) > 0:
                # self.check_double_assigned()
                # print("Need to try again. This was try #{}. Unassigned count:{}".format(i+1, len(unassigned)))
                # print('hard to place delegates: ', hard_to_place)
                iterations += 1
                self.reset_committees()
                # self.check_double_assigned()
                headcount = 0
                for c in self.committee_builders:
                    headcount += len(c.delegates)
                # print('headcount', headcount)
                # self.check_double_assigned()

            else:
                break

        # Save the assignments if all delegates could be assigned
        if len(unassigned) > 0:
            # TODO handle assignment failure on the front end
            return iterations, unassigned
        else:
            for cb in self.committee_builders:
                for delegate in cb.delegates:
                    if cb.committee not in delegate.committee.all():
                        delegate.committee.add(cb.committee)
                        delegate.save()

        committees = Committee.objects.all()
        maximums = {}
        for committee in committees:
            maximums[committee.name] = committee.max_members

        exclude_delegate_ids = []
        for committee in committees:
            committee_builder = CommitteeBuilder(committee, self.adjacent_areas, self.female_percent)
            if len(committee_builder.delegates) >= committee_builder.committee.max_members:
                continue
            delegates = delegates_to_assign.exclude(pk__in=exclude_delegate_ids).order_by('?')
            for delegate in delegates:
                validation_data = committee_builder.validate(delegate, self.escalator_thresholds)
                if all(validation_data.values()):
                    # print('{} added to {}\n'.format(delegate, committee))
                    committee_builder.add_delegates([delegate])
                    exclude_delegate_ids.append(delegate.pk)
                if len(committee_builder.delegates) >= committee_builder.committee.max_members:
                    break

        return iterations, Delegate.objects.filter(committee__isnull=True).count()

    def assign_delegates(self, delegates, iteration=0):
        fail_list = []
        for delegate in delegates:
            for committee_builder in self.committee_builders:
                validation_obj = committee_builder.validate(delegate, self.escalator_thresholds, iteration=iteration)
                if all(validation_obj.values()):
                    committee_builder.delegates.append(delegate)
                    break
            else:
                fail_list.append(delegate.pk)

        return fail_list

    @property
    def female_percent(self):
        total_delegate_count = Delegate.objects.filter(panel__in=self.panels).count()
        if not total_delegate_count:
            female_percent = .5
        else:
            female_count = Delegate.objects.filter(gender='F').filter(panel__in=self.panels).count()
            female_percent = female_count / total_delegate_count
        
        return female_percent

    # @property
    # def total_slots(self):
    #     return reduce(add, Committee.objects.values_list('max_members', flat=True))

    @property
    def committee_composition(self):
        committees = []
        for committee in Committee.objects.all().order_by('display_name'):
            committee_dict = {'name': committee.name, 'display_name': committee.display_name}
            delegates = Delegate.objects \
                .filter(panel__in=self.panels) \
                .filter(committee=committee)
            committee_dict['delegates'] = list(delegates)

            regions = Area.objects \
                .filter(delegates__panel__in=self.panels) \
                .filter(delegates__committee=committee) \
                .values_list('region', flat=True)
            committee_dict['regions'] = sorted(list(regions))
            committee_dict['adjacent_areas'] = committee.adjacent_area_list(self.adjacent_areas)
            committee_dict['gender_counts'] = committee.gender_counts
            committees.append(committee_dict)
        # print(committees)
        return committees


class CommitteeBuilder(object):

    def __init__(self, committee, adjacent_areas, female_percent):
        self.committee = committee
        self.adjacent_areas = adjacent_areas
        self.female_percent = female_percent

        self.max_members = committee.max_members
        self.delegates = list(Delegate.objects.filter(committee__name=self.committee.name))
        self.area_numbers = [delegate.area.areaNumber for delegate in self.delegates]
        self.female_max_base = math.ceil(self.max_members * female_percent)
        self.male_max_base = math.ceil(self.max_members * (1-female_percent))
        self.female_max_adj = self.female_max_base
        self.male_max_adj = self.male_max_base
        self.escalator_thresholds = None

    def validate(self, delegate, escalator_thresholds, iteration=0):
        self.escalator_thresholds = escalator_thresholds
        self.female_max_adj = self.female_max_base + max(0, iteration - escalator_thresholds[Conference.GENDER_KEY])
        self.male_max_adj = self.male_max_base + max(0, iteration - escalator_thresholds[Conference.GENDER_KEY])
        validation_results = {}

        if delegate.gender == 'F':
            validation_results[Conference.GENDER_KEY] = self.female_max_adj >= self.gender_counts[0] + 1
        else:
            validation_results[Conference.GENDER_KEY] = self.male_max_adj >= self.gender_counts[1] + 1

        region_adjustment = max(0, iteration - escalator_thresholds[Conference.REGION_KEY])
        validation_results[Conference.REGION_KEY] = 0 + region_adjustment >= self.same_region_count(delegate)

        recently_assigned_index = self.recent_assignment_index(delegate)
        if recently_assigned_index:
            index_limit = Area.RECENT_COMMITTEE_THRESHHOLD - max(0, iteration - escalator_thresholds[Conference.REGION_KEY])
            validation_results[Conference.RECENTLY_ASSIGNED_KEY] = index_limit < recently_assigned_index
        else:
            validation_results[Conference.RECENTLY_ASSIGNED_KEY] = True

        adjacent_area_count = self.adjacency_count(delegate)
        count_limit = max(0, iteration - escalator_thresholds[Conference.ADJACENCY_KEY])
        validation_results[Conference.ADJACENCY_KEY] = count_limit >= adjacent_area_count

        validation_results[Conference.MAX_MEMBER_KEY] = len(self.delegates) + 1 <= self.max_members

        return validation_results

    @property
    def members(self):
        return self.delegates

    @property
    def gender_counts(self):
        counts = {'F': 0, 'M':0}
        for delegate in self.delegates:
            counts[delegate.gender] += 1

        return (counts['F'], counts['M'])

    def same_region_count(self, delegate):
        return len([d for d in self.delegates if d.area.region == delegate.area.region])

    def recent_assignment_index(self, delegate):
        for index, past_committee_name in enumerate(delegate.area.get_past_committees()):
            if self.committee.name == past_committee_name:
                return index
        else:
            return None

    def adjacency_count(self, new_delegate):
        inclusive_area_numbers = self.area_numbers + [new_delegate.area.areaNumber]
        neighbor_lists = [list(nx.all_neighbors(self.adjacent_areas, area_number)) for area_number in inclusive_area_numbers]
        all_neighbors = set(list(chain.from_iterable(neighbor_lists)))
        committee_areas = set([delegate.area.areaNumber for delegate in self.delegates] + [new_delegate.area.areaNumber])
        # This method of comparison naturally skips one since both areas will be counted if there are two
        # adjacent areas.  Subtracting 1 from the count makes for a smoother scale starting from 0, more
        # amenable to iteration-based adjustments.
        return max(0, len(all_neighbors & committee_areas)-1)

    def add_delegates(self, delegates):
        for delegate in delegates:
            delegate.committee.add(self.committee)
            self.delegates.append(delegate)
        self.calc_committee_properties()

    def calc_committee_properties(self):
        self.area_numbers = [delegate.area.areaNumber for delegate in self.delegates]

    def __str__(self):
        return self.committee.name