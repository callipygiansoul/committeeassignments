import math
from django.db import models
from .constants import STATES, REGIONS
import networkx as nx

# Create your models here.


class Area(models.Model):
    RECENT_COMMITTEE_THRESHHOLD = 4

    areaNumber = models.IntegerField()
    name = models.CharField(max_length=1000)
    state = models.CharField(max_length=2, choices=sorted(STATES.items()))
    region = models.CharField(max_length=2, choices=sorted(REGIONS.items()))
    cycle = models.CharField(max_length=4, choices=(('odd', 'Odd'), ('even', 'Even')))
    past_committees = models.TextField()

    @property
    def max_region_count_per_committee(self):
        cmte_count = Committee.objects.count()
        area_count = Area.objects.filter(region=self.region).count()
        return math.ceil(area_count / cmte_count)

    def get_past_committees(self):
        return self.past_committees.split(',')

    def __str__(self):
        return '{} - {}'.format(self.areaNumber, self.name)


class Committee(models.Model):
    name = models.CharField(max_length=255)
    display_name = models.CharField(max_length=500)
    short_display_name = models.CharField(max_length=500)
    max_members = models.IntegerField()

    @property
    def members(self):
        return self.delegate_set.all()

    @property
    def gender_counts(self):
        female_count = self.members.filter(gender='F').count()
        male_count = self.members.filter(gender='M').count()
        return female_count, male_count
    
    def regions(self, panels):
        region_list = Area.objects \
            .filter(delegate__panel__in=panels) \
            .filter(delegate__committee=self) \
            .values_list('region', flat=True)
        return sorted(list(region_list))

    def adjacent_area_list(self, adjacency_graph):
        delegate_areas = set(Area.objects.filter(delegates__committee=self).values_list('areaNumber', flat=True))
        try:
            # Successful run of this function means there are no neighbors in the delegate_areas subset
            nx.algorithms.mis.maximal_independent_set(adjacency_graph, delegate_areas)
            return []
        except nx.NetworkXUnfeasible:
            adjacent_areas = set()
            for area in delegate_areas:
                adjacent_areas = adjacent_areas | (delegate_areas & set(adjacency_graph.neighbors(area)))
            return adjacent_areas

    def __str__(self):
        return '{} - {}'.format(self.name, self.display_name)


class Delegate(models.Model):
    name = models.CharField(max_length=500)
    area = models.ForeignKey('Area', on_delete=models.PROTECT, related_name='delegates')
    panel = models.IntegerField()
    gender = models.CharField(max_length=1, choices=(('M', 'Male'), ('F', 'Female')))
    committee = models.ManyToManyField('Committee')

    def __str__(self):
        return '{} - {}'.format(self.name, self.area)
