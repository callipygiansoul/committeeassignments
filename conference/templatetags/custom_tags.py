from django import template

register = template.Library()

@register.filter()
def adjacent_areas(adj_set):
    if len(adj_set) == 0:
        return "None"
    else:
        return ', '.join(sorted([str(area) for area in adj_set]))

@register.filter()
def region_count_display(regions):
    region_counts = {}
    unique_regions = sorted(list(set(regions)))
    for region in regions:
        try:
            region_counts[region] += 1
        except KeyError:
            region_counts[region] = 1

    region_list = ['{}({})'.format(key.upper(), region_counts[key]) for key in sorted(region_counts.keys())]
    return ', '.join(region_list)


@register.filter()
def gender_count_display(gender_count_tuple):
    return '{}/{} (F/M)'.format(gender_count_tuple[0], gender_count_tuple[1])


@register.filter()
def assigned_committees_display(committees):
    return ', '.join(sorted([committee.short_display_name for committee in committees]))