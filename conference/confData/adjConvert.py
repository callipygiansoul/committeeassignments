import yaml
import re
# Delete?  Unused?
stateAreas = []
with open('./singleStateAreas.yaml', 'r') as fh:
    try:
        stateAreas = yaml.load(fh)
    except Exception as e:
        print(e)
    
with open ('./adjacent_areas.json') as fh:
    for line in fh:
        # print('before', line)
        match = re.search('([a-zA-Z]{2})', line)
        if match:
            if stateAreas[match.group(1)]:
                line = line.replace(match.group(1), str(stateAreas[match.group(1)]))
            else:
                print('No match for {}'.format(match.group(1)))
                sys.exit()
        print(line, end='')
